from twilio.twiml.messaging_response import MessagingResponse
from flask import Flask, request
from twilio.twiml.voice_response import VoiceResponse, Say

counter = 0
i = 0
app = Flask(__name__)
@app.route('/speak', methods=['GET', 'POST'])
def speak():
	response = '<?xml version="1.0" encoding="UTF-8"?><Response><Say>9 1 1 we have a natural disaster victim, hurt at 555 Peach Street... Because they used this app, we know that they are not able to speak. please hurry.</Say></Response>'
	# response = VoiceResponse()
	# response.say('911 we have a natural disaster victim hurt at 555 Peach Street. Because they used this app, we know that they are not able to speak. please hurry.')
	# res = MessagingResponse()
	# res.message(response)
	return response


@app.route('/', methods=['GET', 'POST'])
def sms():	
   global counter
   inb_message = request.values.get('Body').lower()

   questions = ["Are you currently involved in a natural disaster?",
   "Is this before or after a disaster?",   
   "Do you need to get in touch with your family?", 
   "Did you make a family plan on our app?", 
   "Use the family plan you made in our app to figure out what to do next.",
   "Start over... "
   "Are you currently involved in a natural disaster?",
   "Are you hurt?",
   "Can you dial 911?",
   "Keep Calm! We'll call 911 for You! Calling 911...",
   " ",
   " Do you have an account with us? ",
   "What's your account name?",
   "What's the password?",
   "Welcome back!!! Here are the last known locations of Alicia, Katie, and Natalia" + 
   "Alicia: 654 Grover Road, Katie: 23 85th Street, Natalia: 96 Apple Drive."
   ]
  
   res = MessagingResponse()
   print(counter)
   if counter < 9:
   	res.message(questions[counter])
   	counter = counter + 1 
   	return str(res)

   elif counter == 9:
   	from twilio.rest import Client
   	# Your Account Sid and Auth Token from twilio.com/user/account
   	account_sid = "AC1c5b0731582b41db0f5808bbf35d6e49"
   	auth_token = "c82139c2c6c489160330bb85748ce8bf"
   	client = Client(account_sid, auth_token)
   	call = client.calls.create(
    	to="+19293843313",
    	from_="+17653479467",
    	url="http://c1d6825d.ngrok.io/speak"
	)
	print('trying to call')
	print(call.sid)
	counter = counter + 1
	return str(res)
   
   elif counter > 9 and counter < 14:
   	res.message(questions[counter])
   	counter = counter + 1
   	return str(res)
   else:
   	print('in last else')
   	from twilio.rest import Client
   	account_sid = 'AC1c5b0731582b41db0f5808bbf35d6e49'
   	auth_token = 'c82139c2c6c489160330bb85748ce8bf'
   	client = Client(account_sid, auth_token)
   	message = client.messages \
    	.create(
         	body='Clemone is looking for you! Stick to the family plan! If you forgot it, go to the Dr. Aid app.',
         	from_='+17653479467',
         	to="+19738307004",
         	
    	 )
   message = client.messages \
    	.create(
         	body='Clemone is looking for you! Stick to the family plan! If you forgot it, go to the Dr. Aid app.',
         	from_='+17653479467',
         	to="+19178317644"
         	
    	 )

   message = client.messages \
    	.create(
         	body='Clemone is looking for you! Stick to the family plan! If you forgot it, go to the Dr. Aid app.',
         	from_='+17653479467',
         	to="+19734620276",

    	 )

   print(message.sid)

   return str(res)


if __name__ == "__main__":
   app.run(debug=True)