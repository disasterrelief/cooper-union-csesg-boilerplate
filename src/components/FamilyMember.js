import React from "react";
import Header from "../components/header.js";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import AddIcon from "@material-ui/icons/Add";
import Paper from '@material-ui/core/Paper';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';

class FamilyMember extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      numOn: 1,
			times: [],
			options: [
				<div id='1'>
        <TextField
          id="name"
          label="Name"
          value={this.props.profile.name}
          onChange={(event) => {this.handleChange(event, 'name');}}
          margin="normal"
        />
        <TextField
          id="age"
          label="Age"
          value={this.props.profile.age}
          onChange={(event) => {this.handleChange(event, 'age');}}
          margin="normal"
        />
        </div>,
			],
    };
  }
  addRow(){
		let options2 = this.state.options;
		//let numOn2 = this.state.numOn + 1;
		var addThis =
    <div>
    <TextField
      id="name"
      label="Name"
      value={this.props.profile.name}
      onChange={(event) => {this.handleChange(event, 'name');}}
      margin="normal"
    />
    <TextField
      id="age"
      label="Age"
      value={this.props.profile.age}
      onChange={(event) => {this.handleChange(event, 'age');}}
      margin="normal"
    />
    </div>
		//options2.push(addThis);
		this.setState({
				options: [...this.state.options, addThis]
			})
	}

  handleChange(event, field) {
    let uid = this.props.auth.uid;
      this.props.firebase.update(`/users/${uid}`,
      {[field]:
        event.target.value});
  }
  render(){
    const disasterBox={
    margin: 'auto',
    height: '25%',
    padding: 'auto',
  }
    return(
      <div>
          <h2>What are the names and ages of the members of your party?</h2>
          <button type="button" onClick={() => {this.addRow()}}>+</button> <br />
          <FormControlLabel control={<div>{this.state.options}</div>}/>
          <br />
          <br />
      </div>
    );
  }
}

export default compose(
  firebaseConnect(),
  firebaseConnect((props) => [{path:'name'}]),
  firebaseConnect((props) => [{path:'age'}]),
  firebaseConnect((props) => [{path:'escapeRoute'}]),
  firebaseConnect((props) => [{path:'separatedMeetingPlace'}]),
  firebaseConnect((props) => [{path:'emergencyContactName'}]),
  firebaseConnect((props) => [{path:'emergencyContactPhone'}]),
  firebaseConnect((props) => [{path:'emergencyContactAddress'}]),
  firebaseConnect((props) => [{path:'safeRoom'}]),
  firebaseConnect((props) => [{path:'resources'}]),
  connect(({firebase: {auth}})  => ({auth})),
  connect(({firebase: {profile}}) => ({profile}))
)
(FamilyMember)
