/*
   --------
   import the packages we need
   --------
 */

import React from 'react';
import { connect, Provider } from 'react-redux';
import { createStore, combineReducers, compose } from 'redux';
import { reactReduxFirebase, firebaseReducer } from 'react-redux-firebase';
import firebase from 'firebase';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { MuiThemeProvider } from '@material-ui/core/styles';
import theme from './style/theme';
import initialState from './initialState.json';
import './style/main.css';
import Header from './components/header';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';





/*
   --------
   import your pages here
   --------
 */

import HomePage from './pages/home';
import LoginPage from './pages/LoginPage';
import SignupPage from './pages/SignupPage';
import SandwichesPage from './pages/sandwiches';
import DisasterPlanPage from './pages/disasterPlan';
import ContactPage from './pages/contact';
import BeforePlanPage from './pages/beforePlan';
import DuringPlanPage from './pages/duringPlan';
import AfterPlanPage from './pages/afterPlan';
import FindShelterPage from './pages/findShelter';
//import MapTest from './pages/maptest.js'
import PlanChoice from './pages/disasterPlan2';
import NewsPage from './pages/News';
import GeocodingPage from './pages/Geocoding';


/*
   --------
   configure everything
   --------
 */

const firebaseConfig = {
    /*
       --------
       REPLACE WITH YOUR FIREBASE CREDENTIALS
       --------
     */
     apiKey: "AIzaSyC96DLK35OJKNojxkV00I-zSil8bxm6iak",
     authDomain: "dr-a-8f27b.firebaseapp.com",
     databaseURL: "https://dr-a-8f27b.firebaseio.com",
     projectId: "dr-a-8f27b",
     storageBucket: "",
     messagingSenderId: "882145890002"
};

// react-redux-firebase config
const rrfConfig = {
    userProfile: 'users',
};

// Initialize firebase instance
firebase.initializeApp(firebaseConfig);





/*
   --------
   setup redux and router
   --------
 */


const createStoreWithFirebase = compose(
    reactReduxFirebase(firebase, rrfConfig)
)(createStore);

// Add firebase to reducers
const rootReducer = combineReducers({
    firebase: firebaseReducer
});

const store = createStoreWithFirebase(rootReducer, initialState);


const ConnectedRouter = connect()(Router);



export default class App extends React.Component{
    render(){
              //  <Route exact path="/maptest" component={MapTest} />
	return(
	    <MuiThemeProvider theme={theme}>
		<Provider store={store}>
			<ConnectedRouter>
			    <div id="container">
				<Grid container
				justify="center">
				    <Grid item sm={12}>
					<Header></Header>
					<Route exact path="/" component={HomePage} />
					<Route exact path="/LoginPage" component={LoginPage} />
					<Route exact path="/SignupPage" component={SignupPage} />
					<Route exact path="/sandwiches" component={SandwichesPage} />
          <Route exact path="/disasterPlan" component={DisasterPlanPage} />
          <Route exact path="/contact" component={ContactPage} />
          <Route exact path="/beforePlan" component={BeforePlanPage} />
          <Route exact path="/duringPlan" component={DuringPlanPage} />
          <Route exact path="/afterPlan" component={AfterPlanPage} />
          <Route exact path="/findShelter"component={FindShelterPage} />
          <Route exact path="/disasterPlan2" component={PlanChoice} />
            <Route exact path="/News" component={NewsPage} />
            <Route exact path="/Geocoding" component={GeocodingPage} />
				    </Grid>
				</Grid>
			    </div>
			</ConnectedRouter>
		</Provider>
	    </MuiThemeProvider>
	);
    }
}
