import { createMuiTheme } from '@material-ui/core/styles';
import deepPurple from '@material-ui/core/colors/deepPurple';
import grey from '@material-ui/core/colors/grey';


const theme = createMuiTheme({
  palette: {
    primary: deepPurple,
  secondary: grey
  },
});

export default theme;
