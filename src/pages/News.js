import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';

class NewsPage extends React.Component{
  render() {
  return(
    <body>
<h2>How to Deal with the Stresses of Natural Disasters</h2>
<p><a href="http://www.mentalhealthamerica.net/conditions/coping-stress-natural-disasters"><u>Coping with Natural Disaster Stresses</u></a></p>


<p> <a href="https://www.verywellmind.com/coping-with-natural-disasters-2797570"><u>Healthy Methods of Recovery from Trauma Post Natural Disaster</u></a></p>

<h2>Safety from Natural Disasters</h2>
<p><a href="https://www.safehold.com/programs/Pages/naturaldisaster.aspx"><u>Protective Actions Based on Natural Disasters</u></a></p>

<h2>Frequently Asked Questions (FAQ)</h2>
<p><a href="https://www.safehold.com/applicationdownloads/ndpapps/Natural_Disaster_FAQ.pdf"><u>FAQ</u></a></p>

<h2>Dealing with Wildfires</h2>
<p><a href="https://www.nationalgeographic.com/environment/natural-disasters/wildfires/"><u>The Details Behind Wildfires</u></a></p>
<p><a href="https://www.cdc.gov/features/wildfires/index.html"><u>How to Protect Yourself from Wildfire Smoke</u></a></p>
</body>
);
}
}

export default NewsPage
