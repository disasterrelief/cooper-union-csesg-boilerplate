import React from 'react';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import { compose } from 'redux'
import { connect } from 'react-redux'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


class LoginPage extends React.Component{
    constructor(props){
	super(props);
	this.state = {
	    email: '',
	    password: ''
	};
    }

    handleChange(event, field){
	this.setState({
	    [field]: event.target.value
	});
    }

    handleSubmit(event){
    	event.preventDefault();
    	this.props.firebase.login(this.state)
	    .then((response) => {
	    console.log("Login Successful!");
      this.props.history.push("/");
	    })
	    .catch((error) => {
		switch(error.code){
		    case 'auth/user-not-found':
			// do something
			break;
		    case 'auth/wrong-password':
		    case 'auth/invalid-email':
			// do something
			break;
		    case 'auth/network-request-failed':
			// do something
			break;
		    default:
			// default error
		}
	    });
    }

    logout(){
	this.props.firebase.logout();
    }

    render(){
      const loginBox={
      margin: 'auto',
      height: '25%',
      padding: 'auto',
    }
    const top ={
      textAlign: "center",
      margin: '3%',
    }
	let payload;
	if(!this.props.auth.isLoaded){
	    // auth is not warmed up
	    payload = null;
	}
	if(this.props.auth.isLoaded && this.props.auth.isEmpty){
	    // auth is ready
	    // but user is not logged in
	    payload = <div>
      <form onSubmit={(event) => {this.handleSubmit(event);}}>
      <center>
      <Grid xs={10} sm={6} md={5} lg={4} xl={4} justify="center"  style={loginBox}>
          <Paper elevation={5}>
            <br />
            <h1 style={top}>
              Login
            </h1>
      <TextField
        label="Email"
        value={this.state.email}
        onChange={(event) => {this.handleChange(event, 'email');}} />
    <br />
    <br />
		    <TextField
			label="Password"
			type="password"
			value={this.state.password}
			onChange={(event) => {this.handleChange(event, 'password');}}
			margin="normal"
		    />
    <br />
    <br />
		<Button type="submit"
			variant="contained"
			color="primary">
		    Login
		</Button>
    <br/>
    <br/>
    </Paper>
        </Grid>
    </center>
	    </form>
    </div>
	}
	if(this.props.auth.isLoaded && !this.props.auth.isEmpty){
	    // auth is warmed up
	    // and user is not logged in
	    payload = <div>
		<div>
		    Welcome {this.props.auth.email}
		</div>
		<div>
		    <Button variant="contained"
			    color="secondary"
			    onClick={() => {this.props.firebase.logout();}}>
	<Link to="/LoginPage">		Logout </Link>
		    </Button>

		</div>
	    </div>;
	}
	return(
	    <div>
		{payload}
	    </div>
	)
    }
};

export default compose(
    firebaseConnect(),
    connect(({firebase: {auth}}) => ({auth}))
)(LoginPage);
