import React from "react";
import Header from "../components/header.js";
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import AddIcon from "@material-ui/icons/Add";
import Paper from '@material-ui/core/Paper';
import { compose } from 'redux'
import { connect } from 'react-redux'
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import FormControl from '@material-ui/core/FormControl';
import FamilyMember from '../components/FamilyMember.js';
class DisasterPlanPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      age: '',
      escapeRoute:'',
      separatedMeetingPlace:'',
      emergencyContactName:'',
      emergencyContactPhone:'',
      emergencyContactAddress:'',
      safeRoom:'',
      resources:''
  };
  }
  handleChange(event, field) {
    let uid = this.props.auth.uid;
      this.props.firebase.update(`/users/${uid}`,
      {[field]:
        event.target.value});
  }
  render(){
    const disasterBox={
    margin: '5em',
    height: '25%',
    padding: '5em',
  }
    return(
      <div>
        <Paper style={disasterBox}>
          <h1 id="disasterPlanTitle"><center>Answer the following questions to create your <u>Disaster Relief Plan</u></center></h1>
          <br />
          <br />
          <h2>What are the escape routes of your home?</h2>
          <FormControl>
            <TextField
              id="escapeRoute"
              label="Escape Route"
              value={this.props.profile.escapeRoute}
              onChange={(event) => {this.handleChange(event, 'escapeRoute');}}
              margin="normal"
            />
          </FormControl>
          <FamilyMember />
          <br />
          <br />
            <h2>Do You Have Any Resources With You Currently?</h2>
            <FormControl>
                    <TextField
                      id="resources"
                      label="Yes or No"
                      value={this.props.profile.resources}
                      onChange={(event) => {this.handleChange(event, 'resources');}}
                      margin="normal"
                    />
            </FormControl>
                  <br />
                  <br />
            <h2> If separated, where will your family meet?</h2>
            <FormControl>
                  <TextField
                    id="separatedMeetingPlace"
                    label="Separated Meeting Place"
                    value={this.props.profile.separatedMeetingPlace}
                    onChange={(event) => {this.handleChange(event, 'separatedMeetingPlace');}}
                    margin="normal"
                  />
            </FormControl>
                  <br />
                  <br />
              <h2>Who is the emergency contact if your family cannot each other in the immediate area?</h2>
            <FormControl>
            <TextField
              id="emergencyContactName"
              label="Name"
              value={this.props.profile.emergencyContactName}
              onChange={(event) => {this.handleChange(event, 'emergencyContactName');}}
              margin="normal"
            />
            </FormControl>
            <br />
            <FormControl>
            <TextField
              id="emergencyContactPhone"
              label="Phone Number"
              value={this.props.profile.emergencyContactPhone}
              onChange={(event) => {this.handleChange(event, 'emergencyContactPhone');}}
              margin="normal"
            />
            </FormControl>
            <br />
            <FormControl>
            <TextField
              id="emergencyContactAddress"
              label="Address"
              value={this.props.profile.emergencyContactAddress}
              onChange={(event) => {this.handleChange(event, 'emergencyContactAddress');}}
              margin="normal"
            />
            </FormControl>
            <br />

            <h2> What room is your safe room if local authorities direct you to your home?</h2>
            <FormControl>
                    <TextField
                      id="safeRoom"
                      label="Safe Room"
                      value={this.props.profile.safeRoom}
                      onChange={(event) => {this.handleChange(event, 'safeRoom');}}
                      margin="normal"
                    />
            </FormControl>
                    <br />
                    <br />
    <Button type="submit" id="submitButton" color='deepPurple' variant="contained"><Link to="/disasterPlan2">
    Submit</Link>
    </Button>
    </Paper>
      </div>
    );
  }
}

export default compose(
  firebaseConnect(),
  firebaseConnect((props) => [{path:'name'}]),
  firebaseConnect((props) => [{path:'age'}]),
  firebaseConnect((props) => [{path:'escapeRoute'}]),
  firebaseConnect((props) => [{path:'separatedMeetingPlace'}]),
  firebaseConnect((props) => [{path:'emergencyContactName'}]),
  firebaseConnect((props) => [{path:'emergencyContactPhone'}]),
  firebaseConnect((props) => [{path:'emergencyContactAddress'}]),
  firebaseConnect((props) => [{path:'safeRoom'}]),
  firebaseConnect((props) => [{path:'resources'}]),
  connect(({firebase: {auth}})  => ({auth})),
  connect(({firebase: {profile}}) => ({profile}))
)
(DisasterPlanPage)
