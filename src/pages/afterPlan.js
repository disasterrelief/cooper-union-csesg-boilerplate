import React from "react";
import Header from "../components/header.js";
import ReactDOM from 'react-dom';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import Grow from '@material-ui/core/Grow';

class AfterPlanPage extends React.Component {
  state = {
  checked: true,
  };
  handleChange = () => {
    this.setState(state => ({ checked: !state.checked }));
  };
render(){
  const font ={
  fontFamily: 'Raleway',
  }
  const { checked } = this.state;
return(

  <div>
  <Grow in={checked}>
    <h1 style={font} align="center">After Disaster Preparedness Plan</h1>
  </Grow>
  <center>
  <Grow in={checked} style={{ transitionDelay: this.state.checked ? 1000 : 0 }}>
  <Carousel showArrows={true} showThumbs={false} dynamicHeight = {true} width = "450px" autoPlay={true}>
      <div>
          <img src={require("../images/contact.png")}/>
          <p style={font} className="legend">Contact Family & Friends to make sure they are safe</p>
      </div>
      <div>
          <img src={require("../images/shelterMap.PNG")}/>
          <p style={font} className="legend">2. Use our shelter map to find the nearest shelter near you (add link) </p>
      </div>
      <div>
          <img src={require("../images/valuables.jpg")}/>
          <p style={font} className="legend">3. Gather the undamaged valuables in your house. Check to see if your chosen shelter accepts outside belongings</p>
      </div>
  </Carousel>
  </Grow>
  </center>
  </div>
)

}


}

export default AfterPlanPage
