import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'

class InputPage extends React.Component{
    state = {
      name: '',
      age: '',
      numberOfMembers: '',
    };
    andleChange = name => event => {
      this.setState({
        [name]: event.target.value,
      })
    };

    render(){
      const { classes } = this.props;

	return(
    <h1 id="header" align="center">
		   Add member
		</h1>

	    <div>
      <TextField
      id="numberOfMembers"
      label="Number of Members"
      className={classes.textField}
      value ={this.state.age}
      onChange= {this.handleChange('numberOfMembers')}
      />

		<TextField
    id="name"
    label="Name"
    className={classes.textField}
    value ={this.state.name}
    onChange= {this.handleChange('name')}
    />

    <TextField
    id="age"
    label="Age"
    className={classes.textField}
    value ={this.state.age}
    onChange= {this.handleChange('age')}
    />

    <Button component={Link} to="/InputPage">
  Members
    </Button>

	    </div>
	)
    }
};

export default InputPage
