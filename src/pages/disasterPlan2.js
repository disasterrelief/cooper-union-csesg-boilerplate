import React from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { Link } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';


class SimpleMenu extends React.Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleClose = () => {
    this.setState({ anchorEl: null });
  };

  render() {
    const loginBox={
    margin: 'auto',
    height: '25%',
    padding: 'auto',
  }
  const top ={
    textAlign: "center",
    margin: '3%',
  }

    const { anchorEl } = this.state;

    return (
      <div id="createPlan2">
      <center>
      <Grid xs={10} sm={6} md={5} lg={4} xl={4} justify="center"  style={loginBox}>
          <Paper elevation={5}>
            <br />
      <h2>What Plan Do You Want to Create?</h2>
      <br />
      <br />
        <Button
          aria-owns={anchorEl ? 'simple-menu' : null}
          aria-haspopup="true"
          onClick={this.handleClick}
        >
          Select Your Plan
        </Button>
          <br />  <br />
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}><Link to="/beforePlan">Before Event</Link></MenuItem>
          <MenuItem onClick={this.handleClose}><Link to="/duringPlan">During Event</Link></MenuItem>
          <MenuItem onClick={this.handleClose}><Link to="/afterPlan">Aftermath</Link></MenuItem>
        </Menu>
        <br />
        </Paper>
            </Grid>
        </center>
      </div>
    );
  }
}

export default SimpleMenu;
