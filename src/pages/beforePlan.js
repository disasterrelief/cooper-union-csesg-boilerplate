import React from "react";
import Header from "../components/header.js";
import ReactDOM from 'react-dom';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import IconButton from '@material-ui/core/IconButton';

var supplies = ['Medical Supplies/ First Aid Kit', 'Medications', 'Non-Perishable Foods' ,'Water', 'Blankets', 'Clothing', 'Batteries', 'Flashlights', 'Matches', 'Hand Warmers', 'Toiletries', 'Hand Wipes', 'Cash'];
var documents = ['Birth Certificates', 'Marriage Certificates', 'Tax Records' ,'Credit Card Number', 'Financial Records', 'Wills', 'Trusts', 'Medical Information', 'Bank Statements', 'Utility Bills'];
class BeforePlanPage extends React.Component {
  state = {
    checked: [0],
  };
  handleToggle = value => () => {
   const { checked } = this.state;
   const currentIndex = checked.indexOf(value);
   const newChecked = [...checked];

   if (currentIndex === -1) {
     newChecked.push(value);
   } else {
     newChecked.splice(currentIndex, 1);
   }

   this.setState({
     checked: newChecked,
   });
 };
  render() {
    const font ={
    fontFamily: 'Raleway',
    }
    const { classes } = this.props;
          return (
          <div>
          <div>
            <h1 style={font} align="center">Before Disaster Preparedness Plan</h1>
            <center>
              <Carousel showArrows={true} showThumbs={false} dynamicHeight = {true} width = "450px">
                  <div>
                       <img src={require("../images/disasterSupplyKit.jpg")}/>
                      <p style={font} className="legend">1. Create or Buy a Disaster Supply Kit. <br></br> Refer to checklist below to keep track of these items</p>
                  </div>
                  <div>
                      <img src={require("../images/safeBox.jpg")}/>
                      <p style={font} className="legend">2. Create a Safe Box in your House.<br></br> Refer to checklist below to keep track of these items.</p>
                  </div>
                  <div>
                      <img src={require("../images/naturalDisasters.jpg")}/>
                      <p style={font} className="legend">3. Research the Common Natural Disasters Seen<br></br> Within Your Local Community <br></br> e.g. <b>Tornados, Hurricanes, Wildfires, Floods</b></p>
                  </div>
                  <div>
                      <img src={require("../images/alarm.jpg")}/>
                      <p style={font} className="legend">4. Find Out if Your Community Has a Public Warning System</p>
                  </div>
                  <div>
                      <img src={require("../images/meetingPlace.jpg")}/>
                      <p style={font} className="legend">5. Pick a Meeting Place <br></br>e.g Kitchen, Deck, Family Room, etc</p>
                  </div>
                  <div>
                       <img src={require("../images/fullTankGas.jpg")}/>
                      <p style={font} className="legend">8. Keep your gas tank filled <br></br> in case of evacuation.</p>
                  </div>
                  <div>
                      <img src={require("../images/escapeRoute.jpg")}/>
                      <p style={font} className="legend">6. Identify Escape Routes <br></br>Can be created by family or by authorities</p>
                  </div>
                  <div>
                       <img src={require("../images/emergencyNumber.jpg")}/>
                      <p style={font} className="legend">7. Be Aware of local Emergency Numbers <br></br> Police: 911, Firefighters/Ambulance: Vary based on Location</p>
                  </div>
                  <div>
                       <img src={require("../images/fireDetector.png")}/>
                      <p style={font} className="legend">8. Install Smoke Alarms <br></br> Replace/Test Every Three Months</p>
                  </div>
              </Carousel>
              </center>
        </div>

        
        <div>
        <h3 style={font}>Disaster Supply Kit Checklist</h3>
        <div className={classes.root}>
        <List>
          {[0, 1, 2, 3,4,5,6,7,8,9,10,11,12].map(value => (
            <ListItem
              key={value}
              role={undefined}
              dense
              button
              onClick={this.handleToggle(value)}
              className={classes.listItem}
            >
              <Checkbox
                checked={this.state.checked.indexOf(value) !== -1}
                tabIndex={-1}
                disableRipple
              />
              <ListItemText primary={supplies[value]} />
              <ListItemSecondaryAction>
              </ListItemSecondaryAction>
            </ListItem>
          ))}
        </List>
      </div>
      </div>
    }
    <div>
    <h3>Safe Box Checklist</h3>
    <div className={classes.root}>
    <List>
      {[0, 1, 2, 3,4,5,6,7,8,9].map(value => (
        <ListItem
          key={value}
          role={undefined}
          dense
          button
          onClick={this.handleToggle(value)}
          className={classes.listItem}
        >
          <Checkbox
            checked={this.state.checked.indexOf(value) !== -1}
            tabIndex={-1}
            disableRipple
          />
          <ListItemText primary={documents[value]} />
          <ListItemSecondaryAction>
          </ListItemSecondaryAction>
        </ListItem>
      ))}
    </List>
  </div>
  </div>
    </div>
);
}
}
BeforePlanPage.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(BeforePlanPage);
