import React, { Component } from 'react';
import {Map, InfoWindow, Marker, GoogleApiWrapper} from 'google-maps-react';
import { GoogleMap, KmlLayer, withScriptjs, withGoogleMap, } from "react-google-maps";
import { compose, withProps } from 'recompose';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';

class FindShelterPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showingInfoWindow: false,
      activeMarker: {},
      selectedPlace: {}
    }
    // binding this to event-handler functions
    this.onMarkerClick = this.onMarkerClick.bind(this);
    this.onMapClick = this.onMapClick.bind(this);
  }

  onMarkerClick = (props, marker, e) => {
    this.setState({
      selectedPlace: props,
      activeMarker: marker,
      showingInfoWindow: true
    });
  }

  onMapClick = (props) => {
    if (this.state.showingInfoWindow) {
      this.setState({
        showingInfoWindow: false,
        activeMarker: null
      });
    }
  }

  render()
  {
    const style = {
      'marginLeft': 'auto',
      'marginRight': 'auto'
    }
    const shelterBox={
      margin: 'auto',
      height: '25%',
      padding: '4em',
    }
    const MapWithAKmlLayer = compose(
      withProps({
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSyAFultYjKR5xdsMaLmsu4FqFVluXQ8u48k&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `700px` }} />,
        mapElement: <div style={{ height: `100%` }} />,
      }),
      withScriptjs,
      withGoogleMap
    )(props =>
      <GoogleMap
        defaultZoom={9}
        defaultCenter={{ lat: 41.9, lng: -87.624 }}
      >
        <KmlLayer
          url="https://firebasestorage.googleapis.com/v0/b/dr-a-8f27b.appspot.com/o/1.0_week_age_animated_link.kml?alt=media&token=1cee40d4-a603-41b6-87ad-041bc3de85dd"
          options={{ preserveViewport: true }}
        />
      </GoogleMap>
    );
    let payload;
    if(!isLoaded(this.props.users)){
      // still waiting for connection
      payload = null;
    }
    if(isLoaded(this.props.users) && !isEmpty(this.props.users)){
      payload = Object.keys(this.props.users).map((key) => {
        let user = this.props.users[key];
        var result = ""
        result = result.substring(0,result.length - 2);
        console.log(result);
        if(user.role == 'shelter'){
          return <li key={key} style={{ listStyleType: "none" }}>
            <Paper style={shelterBox} elevation={5}>
              <Typography variant="headline" gutterBottom>
                Name - {user.name}
              </Typography>
              <Typography variant="subheading" gutterBottom>
                Location - {user.location}
              </Typography>
              <Link to ="/contact" id="contact">
                <Button variant="contained" color="inherit">Get in contact!</Button>
                  <br />
                    <br />
                <p2>Has room for big families</p2>
              </Link>
          </Paper>
          <Paper style={shelterBox} elevation={5}>
            <Typography variant="headline" gutterBottom>
              Name - Katie S
            </Typography>
            <Link to ="/contact" id="contact">
              <Button variant="contained" color="inherit">Get in contact!</Button>
                <br />
                  <br />
              <p2>Limited space, approximately 20 slots left</p2>
            </Link>
          </Paper>
          <Paper style={shelterBox} elevation={5}>
            <Typography variant="headline" gutterBottom>
              Name - Natalia
            </Typography>
            <Link to ="/contact" id="contact">
              <Button variant="contained" color="inherit">Get in contact!</Button>
              <br />
                <br />
              <p2> Accepts donations of blankets and pillows</p2>
            </Link>
        </Paper>
        <Paper style={shelterBox} elevation={5}>
          <Typography variant="headline" gutterBottom>
            Name - Clementine
          </Typography>
          <Link to ="/contact" id="contact">
            <Button variant="contained" color="inherit">Get in contact!</Button>
              <br />
                <br />
            <p2>Does not accept any pets</p2>
          </Link>
      </Paper>
          </li>
    }
    else {
            return ''
          }
        });
      }
    return(
      <div>
      <MapWithAKmlLayer />
      <br />
        {payload}
      <br />
      </div>
    )
  }
  };

  export default compose(
    firebaseConnect((props) =>
    [{path: 'users'}
  ]),
  connect((state, props) => ({
    users: state.firebase.data.users
  })),
)
(FindShelterPage)
