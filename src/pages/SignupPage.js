import React from 'react';
import { compose } from 'redux'
import { connect } from 'react-redux'
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import { firebaseConnect, isLoaded, isEmpty } from 'react-redux-firebase'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { Link } from 'react-router-dom';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';
import RadioButtonUncheckedIcon from '@material-ui/icons/RadioButtonUnchecked';
import RadioButtonCheckedIcon from '@material-ui/icons/RadioButtonChecked';
import RadioGroup from '@material-ui/core/RadioGroup';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';

class SignupPage extends React.Component{
    constructor(props){
	super(props);
	this.state = {
     name: '',
     email: '',
     password: '',
     role: '',
     showPassword: false,
   };
}


 handleChange(event , field){
   this.setState({
     [field]: event.target.value
   });
 }

 handleRoleChange = event => {
  console.log(this.state.role);
  this.setState({ role: event.target.value });
}

 handleSubmit(event){
   event.preventDefault();

   const credentials = {
     email: this.state.email,
     password: this.state.password,
   }
   const profile = {
     name: this.state.name,
     email: this.state.email,
     role: this.state.role,
   }

   this.props.firebase.createUser(credentials,profile)
    .then((response) => {
      console.log("Account Created!");
      this.props.history.push("/");
   })
   .catch((error) => {
     console.log(error);
     switch(error.code){
       case 'auth/email-already-in-use':
       alert('email is already in use')
       break;
       case 'auth/invalid-email':
       alert('Enter a valid email')
       break;
       case 'auth/operation-not-allowed':
       break;
       case 'auth/weak-password':
       alert('Enter a stronger password')
       break;
       default:
     }
   });
 }

 handleMouseDownPassword = event => {
   event.preventDefault();
 };

 handleClickShowPassword = () => {
   this.setState(state => ({ showPassword: !state.showPassword }));
 };

    render(){
      const top ={
      textAlign: "center",
      margin: '3%',
      fontFamily: 'Raleway',
    }

    const signUpBox={
      margin: 'auto',
      height: '25%',
      padding: 'auto',
    }
    const infotext = `
    If you are in need of assistance of food, water, or shelter, register as a citizen. If you are an organization offering
    hospitality, register as a shelter. If you are a citizen who is offering hospitality, register as a volunteer.
    `;
	let payload;
	if(!this.props.auth.isLoaded){
	    // auth is not warmed up
	    payload = null;
	}
	if(this.props.auth.isLoaded && this.props.auth.isEmpty){
	    // auth is ready
	    // but user is not logged in
	    payload = <div>
      <form onSubmit={(event) => {this.handleSubmit(event);}}>
          <center>
          <br />
            <Grid xs={10} sm={6} md={5} lg={4} xl={4} justify="center"  style={signUpBox}>
                <Paper elevation={5}>
                  <br />
                  <h1 style={top}>
                    Signup
                  </h1>
    		      <TextField
    			    label="Full Name"
    			    value={this.state.name}
    			    onChange={(event) => {this.handleChange(event, 'name');}}
    		      />
           <br />
           <br />
		<FormControl >
		    <TextField
			label="Email"
			value={this.state.email}
			onChange={(event) => {this.handleChange(event, 'email');}}
			margin="normal"
		    />
        </FormControl>
        <br />
        <br />
        <FormControl >
        <TextField
          label="Password"
          type="password"
          value={this.state.password}
          onChange={(event) => {this.handleChange(event, 'password');}}/>
       </FormControl>
       <br />
       <br />
       <FormControl>
          <FormLabel>
            Role
          </FormLabel>
          <RadioGroup
            aria-label="Role"
            name="role"
            value={this.state.role}
            onChange={this.handleRoleChange}
            style={{display: 'flex', flexDirection: 'row'}}
          >
            <FormControlLabel value="citizen" control={<Radio color="primary"/>} label="Citizen" />
            <FormControlLabel value="shelter" control={<Radio color="primary"/>} label="Shelter" />
            <FormControlLabel value="volunteer" control={<Radio color="primary"/>} label="Volunteer" />
          </RadioGroup>
        </FormControl>
      <br />
     <br />
     <Button
      type="submit"
      variant="raised"
      color="primary">
      Submit
       </Button>
      <br />
       <br />
      <h2><font size="5">Already have an account? <Link to="/LoginPage"><u>Login!</u></Link></font></h2>
      <br />
       <br />
      </Paper>
      </Grid>
    </center>
	  </form>
</div>
}
	if(this.props.auth.isLoaded && !this.props.auth.isEmpty){
	    // auth is warmed up
	    // and user is not logged in
	    payload = <div>
    	<div>
		    Welcome {this.props.auth.email}
		</div>
		<div>
		    <Button variant="contained"
			    color="secondary">
			Logout
		    </Button>

		</div>
	    </div>;
	}
	return(
	    <div>
		{payload}
	    </div>
	)
}
};

export default compose(
  firebaseConnect(),
  connect(({firebase: {auth}}) => ({auth})))
(SignupPage);
