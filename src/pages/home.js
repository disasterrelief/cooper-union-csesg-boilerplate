import React from 'react';
import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom'
import { Parallax, Background } from 'react-parallax';
import Typography from '@material-ui/core/Typography';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ButtonBase from '@material-ui/core/ButtonBase';
import Grid from '@material-ui/core/Grid';
import Grow from '@material-ui/core/Grow';


const buttonStyle =
{
   padding: '1em 2em',
   margin: '6em',
   textDecoration: 'none',
}

class HomePage extends React.Component{
  state = {
    checked: true,
  };

  handleChange = () => {
    this.setState(state => ({ checked: !state.checked }));
  };

    render(){
      const { checked } = this.state;
	return(
	    <div >
      <div class="Parallax">
      <center>
      <br/>
      <br/>
      <Grow in={checked}>
      <Typography variant="display4" id="description" gutterBottom>
      Dr.Aid
      </Typography>
      </Grow>
      </center>
      <Grow in={checked} style={{ transitionDelay: this.state.checked ? 500 : 0 }}>
        <h3 id="description" align="center">
    		    Dr. Aid is a communication webapp that helps individuals that are about to be hit by a natural disaster, are currently experiencing a natural disaster, or facing the devastating consequences of a natural disaster that has already hit. Utilizing the user's location, our app will calculate the closest shelter and provide directions and ETA. Along with the map, our program allows users to create a Safety Plan to better prepare them for future natural disasters, as well as help them deal with the dreadful aftermath of a catastrophe that has already hit.'
		</h3>
    </Grow>
    </div>
    <Grid item xs={12}>
      <center>
        <Link to="/disasterPlan">
          <Button style = {buttonStyle} variant="contained" color="primary">
          Create & View Safety Plan
          </Button>
        </Link>
        <Link to="/mapTest">
          <Button style = {buttonStyle} variant="contained" color="primary">
          Find Shelter
          </Button>
        </Link>
        <Link to="/News">
          <Button style = {buttonStyle} variant="contained" color="primary">
          News
          </Button>
        </Link>
        <Link to="/contact">
          <Button style = {buttonStyle} variant="contained" color="primary">
          Contact
          </Button>
        </Link>
        </center>
      </Grid>
      <br />
      <br />
	    </div>
	);
    }
}

export default HomePage
