import React from "react";
import Header from "../components/header.js";
import ReactDOM from 'react-dom';
import { Carousel } from 'react-responsive-carousel';
import styles from 'react-responsive-carousel/lib/styles/carousel.min.css';

class DuringPlanPage extends React.Component {
render(){
return(
  <div>
  <h1 align="center">During Disaster Preparedness Plan</h1>
  <center>
  <Carousel showArrows={true} showThumbs={false} dynamicHeight = {true} width = "450px" autoPlay={true}>
      <div>
          <img src={require("../images/turnOff.png")}/>
          <p className="legend">1. Turn off gas, water, electricity, and other utilities if proven a risk to your safety</p>
      </div>
      <div>
          <img src={require("../images/obtain.jpg")}/>
          <p className="legend">2. Obtain the Disaster Supply Kit and Safe Box</p>
      </div>
      <div>
          <img src={require("../images/contact.png")}/>
          <p className="legend">3. Contact friends/family to meet (use user input info from questions) </p>
      </div>
      <div>
          <img src={require("../images/assistance.png")}/>
          <p className="legend">4. Assist elderly, kids, or pets in your family </p>
      </div>
      <div>
          <img src={require("../images/evacuation.jpg")}/>
          <p className="legend">5. Listen to local authority Evacuation vs Lockdown If evacuating, wear protective clothes, use escape routes (use user input info from questions) </p>
      </div>

  </Carousel>
  </center>
  </div>
);
}
}
export default DuringPlanPage
